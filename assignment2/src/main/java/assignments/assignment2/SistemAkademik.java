package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private static Mahasiswa siswa;
    private static MataKuliah course;

    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm){ // Mencari objek mahasiswa disimpan di siswa
        for (int i = 0; i < 100; i++){
            if (daftarMahasiswa[i].getNPM() == npm){
                siswa = daftarMahasiswa[i];
                return null;
            }
        }
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah){ // Mencari objek MataKuliah disimpan di course
        for (int i = 0; i < 100; i++){
            if (daftarMataKuliah[i] == null) continue;
            if (daftarMataKuliah[i].toString().equals(namaMataKuliah)){
                course = daftarMataKuliah[i];
                return null;
            }
        }
        return null;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        siswa = getMahasiswa(npm);

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i + 1 + " : ");
            String namaMataKuliah = input.nextLine();
            getMataKuliah(namaMataKuliah);
            siswa.addMatkul(course); // Menambahkan menghubungkan objek MataKuliah dengan Mahasiswa
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        getMahasiswa(npm);

        if (siswa.getJumlahMatkul() == 0) {
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
            return;
        }

        System.out.print("Banyaknya Matkul yang Di-drop: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang di-drop:");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i + 1 + " : ");
            String namaMataKuliah = input.nextLine();
            getMataKuliah(namaMataKuliah);
            siswa.dropMatkul(course);
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        getMahasiswa(npm);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + siswa.toString());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + siswa.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        int jumlahMatkul = siswa.getJumlahMatkul();
        if (jumlahMatkul == 0){
            System.out.println("Belum ada mata kuliah yang diambil");
        } else {
            for (int i = 0; i < jumlahMatkul; i++){
                System.out.println(i + 1 + ". " + siswa.getMatkul(i).toString());
            }
        }

        System.out.println("Total SKS: " + siswa.getTotalSks());

        System.out.println("Hasil Pengecekan IRS:");

        siswa.cekIRS();
        if (siswa.getJumlahMasalah() == 0){
            System.out.println("IRS tidak bermasalah.");
        } else {
            for (int i = 0; i < siswa.getJumlahMasalah(); i++){
                System.out.println((i + 1) + ". " + siswa.getMasalah(i));
            }
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        getMataKuliah(namaMataKuliah);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + course.toString());
        System.out.println("Kode: " + course.getKode());
        System.out.println("SKS: " + course.getSks());
        System.out.println("Jumlah mahasiswa: " + course.getKapasitasTerisi());
        System.out.println("Kapasitas: " + course.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");

        if (course.getKapasitasTerisi() == 0){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        } else {
            for (int i = 0; i < course.getKapasitasTerisi(); i++){
                System.out.println(i + 1 + ". " + course.getSiswa(i));
            }
        }
    }

    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            /* Buat instance mata kuliah dan masukkan ke dalam Array */
            daftarMataKuliah[i] = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            /* Buat instance mahasiswa dan masukkan ke dalam Array */
            daftarMahasiswa[i] = new Mahasiswa(dataMahasiswa[0], npm);
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }    
}