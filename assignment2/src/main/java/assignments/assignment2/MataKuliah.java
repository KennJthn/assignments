package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;

    private int kapasitasTerisi = 0;

    public MataKuliah(String kode, String nama, int sks, int kapasitas) {
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        daftarMahasiswa[kapasitasTerisi] = mahasiswa;
        kapasitasTerisi++;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int counter = 0;
        Mahasiswa[] copyArray = new Mahasiswa[kapasitas];
        for (int i = 0; i < kapasitas; i++){
            Mahasiswa siswa = daftarMahasiswa[i];
            if (siswa == null) continue;
            if (mahasiswa.getNPM() != siswa.getNPM()){
                copyArray[counter] = siswa;
                counter++;
            }
        }
        daftarMahasiswa = copyArray;
        daftarMahasiswa[kapasitas-1] = null; // ga guna
        kapasitasTerisi--;
    }

//################################################################
// Getter kode, nama, kapasitas, dan sks;
    public String getKode() {
        return kode;
    }

    public String toString() {
        return nama;
    }

    public int getSks() {
        return sks;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public int getKapasitasTerisi() {
        return kapasitasTerisi;
    }

    public Mahasiswa getSiswa(int i){
        return daftarMahasiswa[i];
    }
}
