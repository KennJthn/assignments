package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS = new String[20];
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    private int jumlahMatkul = 0;
    private int jumlahMasalah = 0;

    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        String NPM = String.valueOf(npm); // 1902
        jurusan = NPM.substring(2, 4); // 02

        if (jurusan.equals("01")) jurusan = "IK";
        else if (jurusan.equals("02")) jurusan = "SI";
    }
    
    public void addMatkul(MataKuliah mataKuliah){
        // Kalau udh diambil gabisa ambil lg
        for (int i = 0; i < 10; i++){
            if (mataKuliah == this.mataKuliah[i]){
                System.out.println("[DITOLAK] " + mataKuliah.toString() + " telah diambil sebelumnya.");
                return;
            }
        }
        // Kapasitas full
        if (mataKuliah.getKapasitas() == mataKuliah.getKapasitasTerisi()){
            System.out.println("[DITOLAK] " + mataKuliah.toString() + " telah penuh kapasitasnya.");
            return;
        }
        // Kalau udh 10 gabisa nambah
        if (jumlahMatkul == 10){
            System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
            return;
        }

        totalSKS += mataKuliah.getSks();               // Tambahkan total SKS
        this.mataKuliah[jumlahMatkul] = mataKuliah;     // Tambahkan ke daftar Matkul yang diambil sekarang
        jumlahMatkul++;                                 // Banyak MatKul yang diambil sekarang
        mataKuliah.addMahasiswa(this);                  // Masukkan ke daftar mahasiswa yang mengambil MatKul
    }
    
    public void dropMatkul(MataKuliah mataKuliah){
        boolean cek = false;
        int counter = 0;
        // Kalau ga ngambil itu
        for (int i = 0; i < 10; i++){
            if (mataKuliah == this.mataKuliah[i]){
                cek = true; // Kalau sudah pernah diambil (true)
            }
        }
        if (!cek){ // Kalau belum pernah diambil (false)
            System.out.println("[DITOLAK] " + mataKuliah.toString() + " belum pernah diambil.");
            return;
        }

        MataKuliah[] copyArray = new MataKuliah[10];
        for (int i = 0; i < 10; i++){
            if (mataKuliah != this.mataKuliah[i]){
                copyArray[counter] = this.mataKuliah[i];
                counter++;
            }
        }
        totalSKS -= mataKuliah.getSks();
        jumlahMatkul--;
        this.mataKuliah = copyArray;
        mataKuliah.dropMahasiswa(this);
    }

    public void cekIRS(){ // bisa recycle jumlahMasalah sama counter
        jumlahMasalah = 0;
        int counter = 0;
        String[] copyArray = new String[20];
        // Maks SKS
        if (totalSKS > 24){
            copyArray[counter] = "SKS yang Anda ambil lebih dari 24";
            counter++;
            jumlahMasalah++;
        }
        // Ga sesuai jurusan
        for (int i = 0; i < jumlahMatkul; i++){
            MataKuliah course = mataKuliah[i];
            if (!course.getKode().equals(jurusan) && !course.getKode().equals("CS")) {
                copyArray[counter] = "Mata Kuliah " + mataKuliah[i].toString() + " tidak dapat diambil jurusan " + jurusan;
                counter++;
                jumlahMasalah++;
            }
        }
        masalahIRS = copyArray;
    }

    // Getter nama, NPM, jurusan, jumlah mata kuliah, jumlah sks, dan mata kuliah
    public String toString(){
        return nama;
    }

    public long getNPM(){
        return npm;
    }
    
    public String getJurusan(){
        if (jurusan.equals("IK")){
            return "Ilmu Komputer";
        } else {
            return "Sistem Informasi";
        }
    }

    public int getJumlahMatkul(){
        return jumlahMatkul;
    }

    public int getTotalSks(){
        return totalSKS;
    }

    public MataKuliah getMatkul(int i){
        return mataKuliah[i];
    }

    public int getJumlahMasalah(){
        return jumlahMasalah;
    }

    public String getMasalah(int i){
        return masalahIRS[i];
    }
}
