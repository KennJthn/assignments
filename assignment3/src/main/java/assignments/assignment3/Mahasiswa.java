package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {
    private long npm;
    private String jurusan;
    private String tanggalLahir;
    private int index = 0; // Banyak mata kuliah yang diambil
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];

    public Mahasiswa(String nama, long npm) {
        tipe = "Mahasiswa";
        this.nama = nama;
        this.npm = npm;
        tanggalLahir = extractTanggalLahir(npm);
        jurusan = extractJurusan(npm);
    }

    public void addMatkul(MataKuliah mataKuliah) {
        String matKul = mataKuliah.toString();

        for (int i = 0; i < index; i++) {
            if (mataKuliah == daftarMataKuliah[i]){ // Pernah terdaftar
                System.out.println("[DITOLAK] " + matKul + " telah diambil sebelumnya");
                return;
            }
        }

        if (mataKuliah.getStatus()) { // Kapasitas
            System.out.println("[DITOLAK] " + matKul +
                    " telah penuh kapasitasnya");
        } else { // Berhasil
            daftarMataKuliah[index++] = mataKuliah;
            mataKuliah.addMahasiswa(this);
            System.out.println(nama + " berhasil menambahkan mata kuliah " +
                    matKul);
        }
    }

    public void dropMatkul(MataKuliah mataKuliah) {
        int pointer = 0;
        MataKuliah[] copyArray = new MataKuliah[10];
        for (int i = 0; i < 10; i++) {
            if (mataKuliah == daftarMataKuliah[i]){
                System.out.println(nama + " berhasil drop mata kuliah " + mataKuliah.toString());
                mataKuliah.dropMahasiswa(this);
            } else {
                copyArray[pointer++] = daftarMataKuliah[i];
            }
        }
        if (pointer == 10) { // Tidak ada yang di remove
            System.out.println("[DITOLAK] " + mataKuliah.toString() + " belum pernah diambil");
        } else {
            daftarMataKuliah = copyArray;
            index--;
        }
    }

    private String extractTanggalLahir(long npm) {
        String result = Long.toString(npm); // NPM dalam String
        result = result.substring(4,12); // Extract
        return result;
    }

    private String extractJurusan(long npm) {
        String result = Long.toString(npm); // NPM dalam String
        result = result.substring(2,4);

        // Kode menjadi jurusan yang sesuai
        result = (result.equals("01")) ? "Ilmu Komputer" : "Sistem Informasi";
        return result;
    }

    public String getTanggalLahir() {
        return Integer.parseInt(tanggalLahir.substring(0,2)) + "-" +
                Integer.parseInt(tanggalLahir.substring(2,4)) + "-" +
                Integer.parseInt(tanggalLahir.substring(4));
    }

    public String getJurusan() {
        return jurusan;
    }

    public String getMatkul(int i) {
        return daftarMataKuliah[i].toString();
    }

    public int getIndex() {
        return index;
    }
}