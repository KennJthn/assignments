package assignments.assignment3;

class Makanan {
    private String nama;
    private long harga;

    public Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }

    public String toString() {
        return nama;
    }

    public long getHarga() {
        return harga;
    }
}