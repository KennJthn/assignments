package assignments.assignment3;
import java.util.Scanner;
import java.util.Arrays;

public class Main {
    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static int totalElemenFasilkom = 0;
    private static int totalMataKuliah = 0;

    static void addMahasiswa(String nama, long npm) {
        daftarElemenFasilkom[totalElemenFasilkom++] = new Mahasiswa(nama, npm);
        System.out.println(nama + " berhasil ditambahkan");
    }

    static void addDosen(String nama) {
        daftarElemenFasilkom[totalElemenFasilkom++] = new Dosen(nama);
        System.out.println(nama + " berhasil ditambahkan");
    }

    static void addElemenKantin(String nama) {
        daftarElemenFasilkom[totalElemenFasilkom++] = new ElemenKantin(nama);
        System.out.println(nama + " berhasil ditambahkan");
    }

    static void menyapa(String objek1, String objek2) {
        ElemenFasilkom aktif, pasif;
        aktif = cariObjek(objek1);
        pasif = cariObjek(objek2);
        if (aktif == pasif)
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        else
            aktif.menyapa(pasif);
    }

    static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom elemen = cariObjek(objek);
        if (elemen.getTipe().equals("ElemenKantin")){
            ElemenKantin kantin = (ElemenKantin) elemen;
            kantin.setMakanan(namaMakanan, harga);
        } else {
            System.out.println("[DITOLAK] " + objek + " bukan merupakan elemen kantin");
        }
    }

    static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom pembeli = cariObjek(objek1);
        ElemenFasilkom penjual = cariObjek(objek2);
        if (!penjual.getTipe().equals("ElemenKantin")) {
            System.out.println("[DITOLAK] Hanya elemen kantin yang" +
                    " dapat menjual makanan");
        } else if (pembeli == penjual) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa" +
                    " membeli makanan sendiri");
        } else
            ElemenFasilkom.membeliMakanan(pembeli, penjual, namaMakanan);
    }

    static void createMatkul(String nama, int kapasitas) {
        daftarMataKuliah[totalMataKuliah++] = new MataKuliah(nama, kapasitas);
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
    }

    static void addMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = cariObjek(objek);
        MataKuliah matKul = cariMatKul(namaMataKuliah);

        if (elemen.getTipe().equals("Mahasiswa")) {
            Mahasiswa siswa = (Mahasiswa) elemen;
            siswa.addMatkul(matKul);
        } else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }
    }

    static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = cariObjek(objek);
        MataKuliah matKul = cariMatKul(namaMataKuliah);

        if (elemen.getTipe().equals("Mahasiswa")){
            Mahasiswa siswa = (Mahasiswa) elemen;
            siswa.dropMatkul(matKul);
        } else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        }
    }

    static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = cariObjek(objek);
        MataKuliah matKul = cariMatKul(namaMataKuliah);
        if (elemen.getTipe().equals("Dosen")) {
            Dosen dosen = (Dosen) elemen;
            dosen.mengajarMataKuliah(matKul);
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }
    }

    static void berhentiMengajar(String objek) {
        ElemenFasilkom elemen = cariObjek(objek);
        if (elemen.getTipe().equals("Dosen")) {
            Dosen dosen = (Dosen) elemen;
            dosen.dropMataKuliah();
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
    }

    static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom elemen = cariObjek(objek);
        if (elemen.getTipe().equals("Mahasiswa")) {
            Mahasiswa siswa = (Mahasiswa) elemen;
            System.out.println("Nama: " + siswa.toString());
            System.out.println("Tanggal lahir: " + siswa.getTanggalLahir());
            System.out.println("Jurusan: " + siswa.getJurusan());
            System.out.println("Daftar Mata Kuliah:");
            if (siswa.getIndex() == 0) {
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                for (int i = 0; i < siswa.getIndex(); i++) {
                    System.out.println((i+1) + ". " + siswa.getMatkul(i));
                }
            }
        } else {
            System.out.println("[DITOLAK] " + elemen.toString() + " bukan merupakan seorang mahasiswa");
        }
    }

    static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah matKul = cariMatKul(namaMataKuliah);

        System.out.println("Nama mata kuliah: " + matKul.toString());
        System.out.println("Jumlah mahasiswa: " + matKul.getTerisi());
        System.out.println("Kapasitas: " + matKul.getKapasitas());

        if (matKul.getLectStatus()) {
            System.out.println("Dosen pengajar: " + matKul.getDosen());
        } else {
            System.out.println("Dosen pengajar: Belum ada");
        }

        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
        if (matKul.getTerisi() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil " +
                    "mata kuliah ini");
        } else {
            for (int i = 0; i < matKul.getTerisi(); i++) {
                System.out.println((i+1) + ". " + matKul.getMahasiswa(i));
            }
        }
    }

    static void nextDay() {
        for (int i = 0; i < totalElemenFasilkom; i++) {
            ElemenFasilkom elemen = daftarElemenFasilkom[i];
            if (elemen.getJumlahDisapa()*2 >= totalElemenFasilkom - 1) // (total-1)/2 bukan (total/2)-1
                elemen.setFriendship(elemen.getFriendship() + 10);
            else
                elemen.setFriendship(elemen.getFriendship() - 5);
            elemen.resetMenyapa();
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    static void friendshipRanking() {
        Arrays.sort(daftarElemenFasilkom, 0, totalElemenFasilkom);
        for (int i = 0; i < totalElemenFasilkom; i++) {
            System.out.println((i+1) + ". " + daftarElemenFasilkom[i] +
                    "(" + daftarElemenFasilkom[i].getFriendship() + ")");
        }
    }

    static void programEnd() {
        System.out.println("Program telah berakhir. Berikut " +
                "nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static ElemenFasilkom cariObjek(String nama) {
        for (int i = 0; i < 100; i++) {
            if (daftarElemenFasilkom[i] == null) break;
            if (nama.equals(daftarElemenFasilkom[i].toString())) {
                return daftarElemenFasilkom[i];
            }
        }
        return null;
    }

    public static MataKuliah cariMatKul(String nama) {
        for (int i = 0; i < 100; i++) {
            if (daftarMataKuliah[i] == null) break;
            if (nama.equals(daftarMataKuliah[i].toString())) {
                return daftarMataKuliah[i];
            }
        }
        return null;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}