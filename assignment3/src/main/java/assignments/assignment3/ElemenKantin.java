package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    private int index = 0; // Banyak makanan
    private Makanan[] daftarMakanan = new Makanan[10];

    public ElemenKantin(String nama) {
        tipe = "ElemenKantin";
        this.nama = nama;
    }

    public void setMakanan(String nama, long harga) {
        for (int i = 0; i < index; i++) {
            if (daftarMakanan[i].toString().equals(nama)){ // Sudah didaftar
                System.out.println("[DITOLAK] " + nama + " sudah pernah terdaftar");
                return;
            }
        }
        daftarMakanan[index] = new Makanan(nama, harga);
        System.out.println(this.nama + " telah mendaftarkan makanan " +
                nama + " dengan harga " + daftarMakanan[index++].getHarga());
    }

    public String getMakanan(int i) {
        return daftarMakanan[i].toString();
    }

    public long getHarga(int i) {
        return daftarMakanan[i].getHarga();
    }

    public int getIndex() {
        return index;
    }
}