package assignments.assignment3;
import java.util.Arrays;

abstract class ElemenFasilkom implements Comparable<ElemenFasilkom> {
    private int index = 0; // Jumlah orang yang disapa
    private int friendship;
    protected String tipe;
    protected String nama;
    ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    public void menyapa(ElemenFasilkom elemenFasilkom) {
        boolean listed = false; // Asumsi bisa disapa (Belum ada di list)
        for (ElemenFasilkom sudahDisapa: telahMenyapa) {
            if (sudahDisapa == elemenFasilkom)
                listed = true; // Sudah disapa (Ada di list)
        }

        if (listed){ // Sudah disapa
            System.out.println("[DITOLAK] " + nama + " telah menyapa " +
                    elemenFasilkom.toString() + " hari ini");
        } else { // Belum disapa
            System.out.println(nama + " menyapa dengan " + elemenFasilkom.toString());
            this.addDisapa(elemenFasilkom);
            elemenFasilkom.addDisapa(this); // Ga dikacangin

            if (tipe.equals("Mahasiswa") &&
                    elemenFasilkom.getTipe().equals("Dosen")) {
                Dosen dosen = (Dosen) elemenFasilkom;
                Mahasiswa siswa = (Mahasiswa) this;

                MataKuliah matKul = dosen.getMatkul();
                if (matKul != null)
                    for (int i = 0; i < siswa.getIndex(); i++) {
                        if (siswa.getMatkul(i).equals(matKul)){
                            friendship += 2;
                            dosen.setFriendship(dosen.getFriendship() + 2);
                            break;
                        }
                    }
            } else if (tipe.equals("Dosen") &&
                    elemenFasilkom.getTipe().equals("Mahasiswa")) {
                Dosen dosen = (Dosen) this;
                Mahasiswa siswa = (Mahasiswa) elemenFasilkom;

                MataKuliah matKul = dosen.getMatkul();
                if (matKul != null)
                    for (int i = 0; i < siswa.getIndex(); i++) {
                        if (siswa.getMatkul(i).equals(matKul)){
                            friendship += 2;
                            siswa.setFriendship(siswa.getFriendship() + 2);
                            break;
                        }
                    }
            }
        }
    }

    public void resetMenyapa() {
        telahMenyapa = new ElemenFasilkom[100];
        index = 0;
    }

    public static void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        ElemenKantin kantin = (ElemenKantin) penjual;
        boolean jual = false; // Jual makanan tsb atau tidak
        long harga = 0;
        for (int i = 0; i < kantin.getIndex(); i++) {
            if (kantin.getMakanan(i).equals(namaMakanan)){ // Ditemukan makanan tsb
                harga = kantin.getHarga(i);
                jual = true;
                break;
            }
        }
        if (jual) { // Alternatif friendship pake Integer
            pembeli.setFriendship(pembeli.getFriendship() + 1);
            penjual.setFriendship(penjual.getFriendship() + 1);

            System.out.println(pembeli.toString() + " berhasil membeli " +
                    namaMakanan + " seharga " + harga);
        } else {
            System.out.println("[DITOLAK] " + penjual.toString() +
                    " tidak menjual " + namaMakanan);
        }
    }

    @Override
    public int compareTo(ElemenFasilkom elemen){
        if (friendship == elemen.getFriendship()){ // Compare String
            return toString().compareTo(elemen.toString());
        } else {
            return elemen.getFriendship() - friendship;
        }
    }

    public String toString() {
        return nama;
    }

    public String getTipe(){
        return tipe;
    }

    public int getJumlahDisapa() {
        return index;
    }

    public int getFriendship() {
        return friendship;
    }

    public void setFriendship(int a) {
        if (a > 100) friendship = 100;
        else if (a < 0) friendship = 0;
        else friendship = a;
    }

    public void addDisapa(ElemenFasilkom elemenFasilkom) {
        telahMenyapa[index++] = elemenFasilkom;
    }
}