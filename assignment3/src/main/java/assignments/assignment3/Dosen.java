package assignments.assignment3;

class Dosen extends ElemenFasilkom {
    private MataKuliah mataKuliah;

    public Dosen(String nama) {
        tipe = "Dosen";
        this.nama = nama;
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        if (this.mataKuliah != null) { // Mengajar suatu matkul
            System.out.println("[DITOLAK] " + nama + " sudah mengajar " +
                    "mata kuliah " + this.mataKuliah.toString());
        } else if (mataKuliah.getLectStatus()){ // Sudah ada dosen yang mengajar
            System.out.println("[DITOLAK] " + mataKuliah.toString() + " sudah memiliki dosen pengajar");
        } else {
            mataKuliah.addDosen(this);
            this.mataKuliah = mataKuliah;
            System.out.println(nama + " mengajar mata kuliah " + mataKuliah.toString());
        }
    }

    public void dropMataKuliah() {
        if (mataKuliah != null) { // Mengajar suatu matkul
            mataKuliah.dropDosen();
            System.out.println(nama + " berhenti mengajar " +
                    mataKuliah.toString());
            mataKuliah = null;
        } else { // Tidak mengajar matkul apapun
            // Dalam dokumen soal ada String "[DITOLAK]" tetapi di expected output tidak ada
            System.out.println("[DITOLAK] " + nama +
                    " sedang tidak mengajar mata kuliah apapun");
        }
    }

    public MataKuliah getMatkul() {
        return mataKuliah;
    }
}