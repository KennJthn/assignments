package assignments.assignment3;

class MataKuliah {
    private String nama;
    private int kapasitas;
    private int terisi = 0;
    private Dosen dosen;
    private boolean lect; // Ada lecturer atau tidak
    private Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        daftarMahasiswa[terisi++] = mahasiswa;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int counter = 0;
        Mahasiswa[] copyArray = new Mahasiswa[kapasitas];
        for (int i = 0; i < kapasitas; i++){
            if (!(mahasiswa == daftarMahasiswa[i])){
                copyArray[counter++] = daftarMahasiswa[i];
            }
        }
        daftarMahasiswa = copyArray;
        terisi--;
    }

    public void addDosen(Dosen dosen) {
        lect = true;
        this.dosen = dosen;
    }

    public void dropDosen() {
        lect = false;
        dosen = null;
    }

    public String toString() {
        return nama;
    }

    public String getDosen() {
        return dosen.toString();
    }

    public String getMahasiswa(int i) {
        return daftarMahasiswa[i].toString();
    }

    public boolean getStatus() {
        return kapasitas == terisi;
    }

    public boolean getLectStatus() {
        return lect;
    }

    public int getTerisi() {
        return terisi;
    }

    public int getKapasitas() {
        return kapasitas;
    }
}