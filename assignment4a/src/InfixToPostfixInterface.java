import javax.swing.*;

public class InfixToPostfixInterface {
    public static void main(String args[]) {
        // Membuat Frame Aplikasi dan Menampilkannya
        JFrame frame = new JFrame("Infix->Postfix Evaluator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new InfixToPostfixPanel());
        frame.pack();
        frame.setVisible(true); // Agar kelihatan
        frame.setResizable(false); // Agar estetik
    }
}