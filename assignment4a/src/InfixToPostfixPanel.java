import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class InfixToPostfixPanel extends JPanel {
    private final int WIDTH = 650;
    private final int HEIGHT = 100;
    private JLabel infixLabel;
    private JLabel postfixLabel;
    private JLabel resultLabel;
    private JLabel errorLabel;

    private JTextField infixInput;
    private JLabel postfixResult;
    private JLabel result;
    private JLabel error;

    private InfixToPostfixConverter converter;
    private String rawExpression;

    public InfixToPostfixPanel() {
        // Set display
        setBackground(Color.LIGHT_GRAY);
        setLayout(new GridLayout(4,2));
        setPreferredSize(new Dimension(WIDTH, HEIGHT));

        // UI untuk info mengenai data yang ditampilkan
        infixLabel = new JLabel("Enter infix expression:");
        postfixLabel = new JLabel("Postfix expression:");
        resultLabel = new JLabel("Result:");
        errorLabel = new JLabel("Error Messages:");

        // Tempat input berupa text field berwarna kuning
        infixInput = new JTextField();
        infixInput.setBackground(Color.YELLOW);
        infixInput.addKeyListener(new EnterListener()); // Key listener untuk saat fokus berada di text field

        postfixResult = new JLabel(); // Expression yang sudah di convert dari infix (awalnya kosong)
        result = new JLabel(); // Hasil penghitungan expression (awalnya kosong)
        error = new JLabel("[ ]"); // List error yang terjadi saat aplikasi berjalan

        // Tambahkan sesuai urutan grid layout dengan urutan dari kiri ke kanan, atas ke bawah
        // Input
        add(infixLabel);
        add(infixInput);

        // Hasil convert
        add(postfixLabel);
        add(postfixResult);

        // Hasil perhitungan atau evaluasi
        add(resultLabel);
        add(result);

        // List semua error yang ditangkap
        add(errorLabel);
        add(error);
    }

    private class EnterListener implements KeyListener { // Listener sebagai cue untuk melakukan operasi thd expression
        public void keyPressed(KeyEvent event) {
            if (event.getKeyCode() == KeyEvent.VK_ENTER) { // Key sebagai listener adalah "Enter"
                // Jika dipencet enter, maka:
                rawExpression = infixInput.getText(); // Mengambil input dalam bentuk string

                // Membuat suatu objek converter yang meng-convert ekspresi input
                converter = new InfixToPostfixConverter(rawExpression);
                postfixResult.setText(converter.toString()); // Tampilkan hasil convert
                result.setText(converter.getStringResult()); // Tampilkan hasil operasi
                error.setText("[" + converter.getErrorMessage() + "]"); // Tampilkan error yang ditangkap
            }
        }

        public void keyReleased(KeyEvent e) { }
        public void keyTyped(KeyEvent e) { }
    }
}
