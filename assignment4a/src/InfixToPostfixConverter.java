import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.Stack;

public class InfixToPostfixConverter {
    private ArrayList<String> error = new ArrayList<String>();
    private ArrayList<String> postfixExpression = new ArrayList<String>();

    private Stack<Long> operand = new Stack<Long>();
    private Stack<Operator> operator = new Stack<Operator>();

    // Life hack (Stress free error handler)
    private int operatorCount = 0;
    private int numberCount = 0;

    private String errorMessage = new String();
    private long result = 0L;

    public InfixToPostfixConverter(String rawExpression) {
        try {
            ConvertInfixToPostfix(rawExpression); // Get tokens from rawExpression -> postfixExpression
            eval(postfixExpression);
            result = operand.pop();
        } catch (InvalidExpressionException exception) {
            error.add(exception.getMessage());
        }

        clean(error);

        for (int i = 0; i < error.size(); i++) {
            errorMessage += error.get(i) + ((i == error.size() - 1)? "" : ", ");
        }
    }

    private void ConvertInfixToPostfix(String input) throws InvalidExpressionException {
        // Mendapatkan token-token dari input
        StringTokenizer tokens = new StringTokenizer(input, "*^/+- ()", true);
        String token;

        // Life hack (Stress free error handler), Jumlah kurung yang ada di input
        int openParenthesisCount = 0;
        int closingParenthesisCount = 0;

        try {
            while (tokens.hasMoreTokens()) {
                token = tokens.nextToken();
                if (token.equals(" ")) { // Spasi dibuang
                    continue;
                }
                try { // Jika berupa angka (Tidak menerima double)
                    Long temp = Long.valueOf(token);
                    postfixExpression.add(token); // Langsung output (File TP4)
                    numberCount++;
                } catch (Exception exception) { // Jika selain angka, diasumsikan berupa operator
                    Operator temp = new Operator(token); // Buat objek operator

                    // Program lanjut ke bagian sini jika operator valid ("+", "-", "*", "/", "^")
                    // Menambahkan counter yang sesuai
                    if (temp.getOperation().equals("(")) { // Kurung pembuka
                        openParenthesisCount++;
                    } else if (temp.getOperation().equals(")")) { // Kurung penutup
                        closingParenthesisCount++;
                    } else { // Kurung pembuka dan kurung penutup tidak dihitung sebagai operator
                        // Untuk error handling
                        operatorCount++;
                    }

                    if (operator.empty()) { // Belum ada operasi
                        operator.push(temp);
                    } else if (temp.getOperation().equals(")")) { // Terdapat kurung
                        // Error Kurung kaga ada
                        if (openParenthesisCount > 0) { // Kalau ada kurung pembuka
                            while (!operator.lastElement().getOperation().equals("(")) {
                                // Lakukan semua operasi di dalam kurung
                                postfixExpression.add(operator.pop().getOperation());
                            }
                            operator.pop(); // Kurung buka dibuang dari stack
                            // Counter dikurangi karena sudah mendapat pasangan kurung
                            openParenthesisCount--;
                            closingParenthesisCount--;
                        } else {
                            throw new InvalidExpressionException("Missing open parenthesis");
                        }
                    } else if (operator.lastElement().compareTo(temp) > 0) { // a > b
                        // Operator sebelumnya lebih tinggi precedence-nya
                        if (!operator.lastElement().getOperation().equals("(")) { // Special case karena precedencenya 4
                            while (!operator.empty()) {
                                if (operator.lastElement().getOperation().equals("(")) { // Last element berubah
                                    break;
                                } else if (operator.lastElement().compareTo(temp) >= 0) { // a >= b
                                    postfixExpression.add(operator.pop().getOperation());
                                } else {
                                    break;
                                }
                            }
                        }
                        operator.push(temp);
                    } else if (operator.lastElement().compareTo(temp) < 0) { // a < b
                        // Operator sebelumnya lebih kecil precedence-nya
                        operator.push(temp);
                    } else { // Operator sebelumnya sama precedencenya
                        if (!operator.lastElement().getOperation().equals("^")) { // Perlu dikerjakan dari kiri
                            postfixExpression.add(operator.pop().getOperation());
                        }
                        operator.push(temp);
                    }
                }
            }
        } catch (InvalidExpressionException exception) {
            // Try block di atas dipastikan hanya throw exception class tersebut
            // dari instruksi: Operator temp = new Operator(token);
            // dan jika tidak ditemukan kurung pembuka namun ditemukan kurung tutup (Untuk berhenti evaluasi)
            error.add(exception.getMessage());
        }
        while (!operator.empty()) { // Operasi sisa yang perlu dilakukan otomatis sesuai urutan
            Operator temp = operator.pop();
            if (!temp.getOperation().equals("(")){ // Kasus dimana terdapat kurung berlebih
                // Kurung tutup tidak ditambah ke stack
                postfixExpression.add(temp.getOperation());
            }
        }

        if (openParenthesisCount > 0) { // Jika ada kurung yang tidak mempunyai pasangannya
            throw new InvalidExpressionException("Missing closing parenthesis"); // Pasangannya hilang T-T
        } else if (closingParenthesisCount > 0) {
            throw new InvalidExpressionException("Missing open parenthesis"); // Pasangannya hilang juga T-T
        }

        // A+B+C+D -> jumlahOperator = 3 || [n]; jumlahOperand = 4 || [n+1]; n = jumlah operator
        // Ekspresi valid selalu berselisih 1 antara jumlah operator dan operand (Karena semua operasi binary)
        if (operatorCount >= numberCount) { // Kalau kebanyakan operator
            throw new InvalidExpressionException("Missing operand");
        } else if (numberCount > operatorCount + 1) { // Kalau terlalu banyak operand
            throw new InvalidExpressionException("Missing operators");
        }

    }

    public void eval(ArrayList<String> input) throws InvalidExpressionException{
        for (int i = 0; i < input.size(); i++) { // Evaluasi postfix menggunakan stack
            try { // Kalau berupa angka
                Long temp = new Long(input.get(i));
                operand.push(temp);
            } catch (Exception exception){ // Operator (sudah di handle saat convert)
                Long result = 0L;
                Long b = operand.pop();
                Long a = operand.pop();
                switch (input.get(i)) { // Operasi yang sesuai dengan operator
                    case "+":
                        result = a + b;
                        break;
                    case "-":
                        result = a - b;
                        break;
                    case "*":
                        result = a * b;
                        break;
                    case "/":
                        try { // Kasus dibagi 0
                            result = a / b;
                        } catch (ArithmeticException arithmeticException){
                            throw new InvalidExpressionException("Division by zero");
                        }
                        break;
                    case "^":
                        result = Pow(a,b);
                }
                operand.push(result);
            }
        }
    }

    public Long Pow(Long a, Long b) { // Pow(a, b) = a pangkat b
        Long result = 1L;
        for (int i = 0; i < b; i++) {
            result *= a;
        }
        return result;
    }

    public void clean(ArrayList<String> input) { // Membersihkan error yang tertangkap lebih dari sekali
        int size = input.size();
        try {
            for (int i = 0; i < size; i++) {
                if (input.get(i) == null) {
                    input.remove(i);
                    continue;
                }
                for (int j = i + 1; j < size; j++) {
                    if (input.get(i).equals(input.get(j))) {
                        input.remove(j);
                    }
                }
            }
        } catch (Exception exception) {
            clean(error);
        }
    }

    public String toString() {
        String result = new String();
        for (int i = 0; i < postfixExpression.size(); i++) {
            result += postfixExpression.get(i);
            if (!(i == postfixExpression.size()-1)) {
                result += " ";
            }
        }
        return result;
    }

    public String getStringResult() {
        return String.valueOf(result);
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}