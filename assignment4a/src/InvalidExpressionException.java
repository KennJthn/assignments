public class InvalidExpressionException extends Exception { // Exception untuk semua kasus invalid di aplikasi
    public InvalidExpressionException(String exception) {
        super(exception);
    }
}