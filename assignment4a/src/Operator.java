public class Operator implements Comparable<Operator> { // Suatu objek operator yang memiliki precedence
    private int precedence;
    private String operation;

    public Operator(String stringObject) throws InvalidExpressionException {
        operation = stringObject;

        // Set precedence sesuai operasinya
        if (stringObject.equals("+")) {
            precedence = 1;
        } else if (stringObject.equals("-")){
            precedence = 1;
        } else if (stringObject.equals("*")) {
            precedence = 2;
        } else if (stringObject.equals("/")) {
            precedence = 2;
        } else if (stringObject.equals("^")) {
            precedence = 3;
        } else if (stringObject.equals("(")) {
            precedence = 4;
        } else if (stringObject.equals(")")) {
            precedence = 4;
        } else { // Jika terdapat string yang bukan berupa operator
            throw new InvalidExpressionException("Parse error");
        }
    }

    // Membandingkan 2 objek operator dari precedencenya (a.compareTo(b))
    // Jika a precedencenya lebih tinggi, maka return valuenya +
    // a.compareTo(b) > 0
    // a > b
    public int compareTo(Operator operatorObject) { // a.compareTo(b) -> a-b
        return precedence - operatorObject.getPrecedence();
    }

    public int getPrecedence() {
        return precedence;
    }

    public String getOperation() {
        return operation;
    }
}
