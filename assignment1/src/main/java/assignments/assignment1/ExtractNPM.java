package assignments.assignment1;
import java.util.*;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */
    static int[] kodeJurusan = {1, 2, 3, 11, 12};
    static Map<Integer, String> dict = new HashMap<Integer, String>(){{
        put(1, "Ilmu Komputer");
        put(2, "Sistem Informasi");
        put(3, "Teknologi Informasi");
        put(11, "Teknik Telekomunikasi");
        put(12, "Teknik Elektro");
    }};

    public static int sumDigit(int num){
        int count = 0;
            while (num > 0){ // Jumlah semua digit dari LSB
                count += num%10;
                num /= 10;
            }
        return count;
    }

    public static int unique(String num){
        int sum = 0;
        int length = num.length();
        for (int i = 0; i < length/2; i++){
            sum += (num.charAt(i)-'0') * (num.charAt(length-1-i)-'0');
        }
        sum += num.charAt(6);
        while (sum > 9){ // Selama masih 2 digit
            sum = sumDigit(sum); // Jumlahan semua digit
        }
        return sum;
    }
    public static boolean validate(long npm) {
        int A, B, C, E, check;
        A = (int)(npm / 1_000_000_000_000L);    // 2 Digit paruh ke-1
        B = (int)(npm / 10_000_000_000L % 100); // 2 Digit paruh ke-2
        C = (int)(npm / 100 % 100_000_000);     // 8 digit paruh ke-3
        E = (int)(npm % 10);                    // 1 Digit paruh ke-5 (Terakhir)
                                                // Total 14 Digit

        // Digit NPM (14 Digit)
        if (A < 10) return false; // Minimal 14 Digit
        if (A > 99) return false; // Maksimal 14 digit

        // Umur
        if (2000 + A - (C % 10_000) < 15) return false; // Minimal 15 tahun

        // Unique Code
        check = unique(String.valueOf(npm/10)); // Kecuali digit terakhir
        if (check != E) return false; // Periksa kode unik NPM dengan "check"(kode unik perhitungan)

        // Kode Jurusan
        for (int i: kodeJurusan)
            if (B == i) return true; // Kode jurusan ada
        return false;
    }

    public static String extract(long npm) {
        // TODO: Extract information from NPM, return string with given format
        int A = (int)(npm / 1_000_000_000_000L);
        int B = (int)(npm / 10_000_000_000L % 100);
        String C = String.valueOf(npm / 100 % 100_000_000);
        if (C.length() < 8)
            C = "0" + C; // Jika tanggal lahir kurang dari 10
                         // Masukkan kembali 0 yang dihilangkan
        int tahun = A + 2000;
        String jurusan = dict.get(B);
        String tanggalLahir = C.substring(0, 2) + "-" + C.substring(2, 4)+ "-" + C.substring(4,8);

        return  String.format(
            "Tahun masuk: %d\nJurusan: %d\nTanggal lahir: %d",
            tahun, jurusan, tanggalLahir
            );
        //"Tahun masuk: " + tahun +
        //"\nJurusan: " + jurusan +
        //"\nTanggal Lahir: " + tanggalLahir;
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            if (validate(npm)){
                System.out.println(extract(npm));
            } else {
                System.out.println("NPM tidak valid!");
            }
            // TODO: Check validate and extract NPM
        }
        input.close();
    }
}